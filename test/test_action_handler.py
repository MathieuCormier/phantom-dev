import logging
from pathlib import Path
from unittest.mock import patch

from pytest import fixture

from . import TestConnector, MockedPhantomModule
with MockedPhantomModule():
	from phantom_dev import action_handler


@fixture
def connector():
	with TestConnector() as instance:
		vault_path = Path(instance.tmp_dir.name).joinpath('vault')
		vault_path.mkdir(exist_ok=True)
		with patch.object(
				action_handler.phantom.vault,
				'vault_info',
				lambda _: vault_path,
		):
			yield instance


@fixture
def log_message():
	return 'TEST LOG MESSAGE'


def test_logging(connector, log_message):
	connector.logger.debug(log_message)
	connector.logger.info(log_message)
	connector.logger.warning(log_message)
	connector.logger.error(log_message)
	connector.logger.critical(log_message)
