from phantom_dev.dummy import MockedPhantomModule

with MockedPhantomModule():
	from phantom_dev.client.default_data.connector import (
		Connector as TestConnector)
